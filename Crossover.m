## Copyright (C) 2017 FGFS
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} Crossover (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: FGFS <fgfs@fgfs-Precision-WorkStation-T3500>
## Created: 2017-03-16

function [c_ind1, c_ind2] = Crossover (ind1, ind2)

img1=ind1.img;
img2=ind2.img;
[x, y]=size(img1);

if(rand(1)<0.5)%perform column-wise crossover
  y2=floor(rand(1)*(y-1))+1;
  y2=y/2;

 % c_img1=zeros(x, y);
  c_img1(:, 1:y2)= img1(:, 1:y2);
  c_img1(:,y2+1:y)=img2(:,y2+1:y);
  flip_prob=rand(1);
  if(flip_prob<0.3)
    c_img1=flip(c_img1, 2);
  elseif(flip_prob>0.3 && flip_prob<0.6)
    c_img1=flip(c_img1);
  endif

%  c_img2=zeros(x, y);
  c_img2(:, 1:y2)= img2(:, 1:y2);
  c_img2(:,y2+1:y)=img1(:,y2+1:y);
  flip_prob=rand(1);
  if(flip_prob<0.3)
    c_img2=flip(c_img2, 2);
  elseif(flip_prob>0.3 && flip_prob<0.6)
    c_img2=flip(c_img2);
  endif


  c_ind1=createIndividual;
  c_ind2=createIndividual;

  c_ind1.img=c_img1;
  c_ind2.img=c_img2;
else%perform row-wise crossover
  x2=floor(rand(1)*(x-1))+1;

%  c_img1=zeros(x, y);
  c_img1(1:x2, :)= img1(1:x2, :);
  c_img1(x2+1:x, :)=img2(x2+1:x, :);

 % c_img2=zeros(x, y);
  c_img2(1:x2, :)= img2(1:x2, :);
  c_img2(x2+1:x, :)=img1(x2+1:x, :);

  c_ind1=createIndividual;
  c_ind2=createIndividual;

  c_ind1.img=c_img1;
  c_ind2.img=c_img2;
end
c_ind1.left_parent=ind1.ID;
c_ind1.right_parent=ind2.ID;

c_ind2.left_parent=ind2.ID;
c_ind2.right_parent=ind1.ID;

endfunction
