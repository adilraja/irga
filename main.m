a = imread('a.jpg');
b = imread('b.jpg');

selection_out = Selection(a,b);

[mut_out,val1,val2] = Mutation(selection_out);

test_value_updated_after_mutation = mut_out(val1,val2)

prompt = 'Enter Row Number to be averted :: ';
x = input(prompt);
out_Aversion = Aversion(mut_out,x);
imshow(out_Aversion);