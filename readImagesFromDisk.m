## Copyright (C) 2017 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} readImagesFromDisk (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2017-03-18

function [retval] = readImagesFromDisk (input1, input2)
  myFolder = [pwd filesep 'images/greyscale/']; %the folder containing colored images
  filePattern = fullfile(myFolder, '*.jpg');
  imagefiles = dir(filePattern);          %array containg all the images references
  nfiles = length(imagefiles);            %size of the file 
  images={};
  for index=1:nfiles
    currentfilename = fullfile(myFolder,imagefiles(index).name);     %reading the file names
 %   fprintf(1, 'Now reading %s\n', currentfilename);                 
    currentimage = imread(currentfilename);
  % currentimage = rgb2gray(currentimage);                           %conversion from colored to black and white image
    images{index} = currentimage;                                    %saving images in a cell array
  endfor
  retval=images;
endfunction
