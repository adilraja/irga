function output = createIndividual (row, col, Dataset) %function input is dataset of images and the size of the reference images
%loop for cropping all the images to a new array     
for index=1:length(Dataset)                         
        tmp = Dataset{index};
        [r,c] = size(tmp);
        if r>=row && c>=col
            newimage = tmp(1:r - (r - row),1:c - (c - col));         
            newarray{index} = newimage;
        else
            index = index + 1;
        end
     end
    output = newarray;      %returning the output
end