myFolder = 'E:\programs\MATLAB7\work\Farooq FYP\GA\Picasso'; %the folder containing colored images
filePattern = fullfile(myFolder, '*.jpg');
imagefiles = dir(filePattern);          %array containg all the images references
nfiles = length(imagefiles);            %size of the file 
for index=1:nfiles
   currentfilename = fullfile(myFolder,imagefiles(index).name);     %reading the file names
   fprintf(1, 'Now reading %s\n', currentfilename);                 
   currentimage = imread(currentfilename);
   currentimage = rgb2gray(currentimage);                           %conversion from colored to black and white image
   images{index} = currentimage;                                    %saving images in a cell array
end
Ref_img = imread('ref_img.jpg');                                    %reference image
Ref_img = rgb2gray(Ref_img);
[r,c] = size(Ref_img);                                              

individual_array = createIndividual(r,c,images);                    %function createimages is being caled and it crops the dataset WRT reference image

writingPath = 'E:/grayimage/file%.02d.jpg';                         %destination folder where all the cropped images are written

%writing all the croped image files to specified folder
for index = 1:length(individual_array)
    fprintf(1, 'Now writing file at %s\n', sprintf(writingPath, index));
    imwrite(individual_array{index},sprintf(writingPath, index));
    % call evaluate function
end
