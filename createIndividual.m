## Copyright (C) 2017 FGFS
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} createIndividual (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: FGFS <fgfs@fgfs-Precision-WorkStation-T3500>
## Created: 2017-03-16

function individual = createIndividual (input1, input2)
    individual=struct("img", rand(300, 300), "fitness", 500000, "ID", [0 0], "left_parent", [0 0], "right_parent", [0 0], "isEvaluated", 0);
    %individual.img=rand(300,300);
    %individual.fitness=500000;
endfunction
