This is the IRGA (Image Reconstruction in a Loop) package. This work is the Final Year Project of Muhammad Farooq Arshad. If you like it and feel like using it please cite:

Arshad, Muhammad Farooq, Muhammad Adil Raja, Junaid Akhtar, and Shams Ur Rahman. "Evolution of Mona Lisa with Pablo Picasso's paintings." In Computing, Mathematics and Engineering Technologies (iCoMET), 2018 International Conference on, pp. 1-5. IEEE, 2018.

https://ieeexplore.ieee.org/abstract/document/8346366/citations

To run this algorithm simply invoke imageReconstructionGA.m OR imageReconstructionGAinALoop.m


