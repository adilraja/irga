## Copyright (C) 2017 FGFS
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} imageReconstructionGAinALoop (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: FGFS <fgfs@fgfs-Precision-WorkStation-T3500>
## Created: 2017-04-04

function [all_pops, all_best] = imageReconstructionGAinALoop (numRuns, numGens)
  all_pops=[];
  all_best=[];
  for(i=1:numRuns)
  %  disp("I am here");
  %  disp(i);
    tic;
    printf ("Processed %d Please be patient.\n", i);
    save counter.mat i;
    [population, best] = imageReconstructionGA (numGens);
    time1=toc;
    save elapsedTime.mat time1;
    all_pops=[all_pops; population];
    all_best=[all_best; best];
  endfor
  %filename=['results-' num2str(num) '.mat'];
  save results.mat all_best;
endfunction
