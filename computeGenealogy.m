## Copyright (C) 2017 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} computeGenealogy (@var{input1}, @var{input2})
##computes genealogy of an individual. It needs that individual and the whole run
##from which to compute this. Computation is done upwards from the end of a run.
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2017-04-11

function genealogy = computeGenealogy (individual, run)
  j=individual.ID(1);%returns the generation number for this individual
  while(j>0)
    
  endwhile
endfunction
