function out = Aversion(img,rowNo)
    fullrow = img(rowNo,:);
    fullrow=fullrow(end:-1:1);
    img(rowNo,:) = fullrow;
    out = img;
end