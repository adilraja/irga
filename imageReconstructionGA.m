## Copyright (C) 2017 Adil
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} imageReconstructionGA (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Adil <adil@adil-HP-250-G3-Notebook-PC>
## Created: 2017-03-16

function [population, best] = imageReconstructionGA (runLength, population)


if(exist('population'))
  images=readImagesFromDisk;
  population2=initPopulation(length(images));
  
  for(i=1:length(images))
    imgSize=size(images{i});
    population2(i).img=images{i};
  
  endfor
  population=[population population2];

endif

if(~exist('population'))
  images=readImagesFromDisk;
  population=initPopulation(length(images));
  
  for(i=1:length(images))
    imgSize=size(images{i});
    population(i).img=images{i};
  
  endfor
  population=tagPopulation(population, 0);%zeroth generation
endif


refImg=imread([pwd filesep 'images/ref_img.jpg']);

pkg load image;
refImg=rgb2gray(refImg);
%imshow(refImg);
%figure;

refImgSize=size(refImg);
fitness=[];
population=evaluatePopulation(population, refImg);
population2=[];
%population2=[population2 population];
best=[];

for(i=1:runLength)
  childPopulation=createChildPopulation(population);

  childPopulation=evaluatePopulation(childPopulation, refImg);
  childPopulation=tagPopulation(childPopulation, i);
  
  if(i<8)
    population=increasePopulation(population, childPopulation);
  elseif(mod(i, 100)==1)
        population=increasePopulation(population, childPopulation);
  else
    population=replacement(population, childPopulation);
    if(mod(i, 25)==0 && length(population)>146)
      population=replacement(population(1:length(population)/2), population(length(population)/2+1:length(population)));
    endif
   % population2=[population2 population];
  endif
  
  save numgen.mat i;
  
  for(j=1:length(population))
    fitness(j)=population(j).fitness;
  endfor
  [fit, I]=min(fitness);
 % imshow(population(I).img);
 best=[best population(I)];
end
endfunction
